# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://bitbucket.org/filipzupancic/stroboskop
```

Naloga 6.2.3:
https://bitbucket.org/filipzupancic/stroboskop/commits/385b3d7bc8545eddaa7fb43e76e27041c0fe6e3e

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/filipzupancic/stroboskop/commits/bc70821c9889ac5a96214b0a0c5d90135ee44b02

Naloga 6.3.2:
https://bitbucket.org/filipzupancic/stroboskop/commits/2645557cce6f6ad46d3d4c2abdfec4bd9d76ba29

Naloga 6.3.3:
https://bitbucket.org/filipzupancic/stroboskop/commits/95a91f6b7171c42ad22b16eaceb5ad7eb2e2d496

Naloga 6.3.4:
https://bitbucket.org/filipzupancic/stroboskop/commits/6e0f20d7b4881ed07b3a6e126b5de195e655b646

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/filipzupancic/stroboskop/commits/47de9188511306459ea7baf19ead6fc12d84ec56

Naloga 6.4.2:
https://bitbucket.org/filipzupancic/stroboskop/commits/c1abe820e1e4479e23ae21c6c25d52366ac13b5d

Naloga 6.4.3:
https://bitbucket.org/filipzupancic/stroboskop/commits/03228006afb6770da3bebf4e37385cdfad8b0894

Naloga 6.4.4:
https://bitbucket.org/filipzupancic/stroboskop/commits/6b3b7bd0f3a390bfe609ea87a9840eef66ac5ad3